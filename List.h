#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

// Libraries Includes
#include <stdlib.h>  // For Dynamic Memory Management
#include <stdio.h>   // For Files Interaction

// Defines
#ifndef _BOOL
#define _BOOL

typedef enum boolean
{
	FALSE,
	TRUE
}boolean;

#endif

// Typedef Deceleration
typedef struct nodeLinkedList
{
	int                    line;
	void                  *info;
	struct nodeLinkedList *next;
}nodeLinkedList;

// Function Prototypes
void            init_List(nodeLinkedList **list);

void            addNode_List(nodeLinkedList  *list , void            *info , int              line);

void            deleteNode_List(        nodeLinkedList **list);

void add_Node_By_Line(nodeLinkedList  *list , void *info , int line);

void delete_Node_By_Line(nodeLinkedList  *list , int line);

nodeLinkedList *Find_Node_By_Line(nodeLinkedList  *list , int line);

void            createFromMemoFile_List(nodeLinkedList **list, FILE            *file);

void            saveToFile_List(nodeLinkedList  *list , FILE *file);

void            free_List(nodeLinkedList **list);

nodeLinkedList *getNextNode_List(nodeLinkedList  *list , nodeLinkedList *Node_Ptr);

void            copy_node(nodeLinkedList  *Source , nodeLinkedList  *Destination);

boolean         getNodeByLine_List(nodeLinkedList  *list , nodeLinkedList  *helpNode, int              line);

#endif // LIST_H_INCLUDED
