#include "List.h"


void init_List(nodeLinkedList **list)
{
    // Code Section
    *list = NULL;
}


// InsertAfter
void addNode_List(nodeLinkedList  *list,void *info,int line)
{
    // Make new node
    nodeLinkedList *ptr_temp;
	ptr_temp = (nodeLinkedList*)malloc(sizeof(nodeLinkedList));
	// Set the new node
	ptr_temp->info = info;
	ptr_temp->next = list->next;
	// Connect the new node
	list->next = ptr_temp;
	free(ptr_temp);
}

// DeleteAfter
void deleteNode_List(nodeLinkedList **list)
{
    // Variable Definition
    nodeLinkedList *ptr_temp;

    // Code Section
    // Set temporary node
	ptr_temp = (nodeLinkedList*)malloc(sizeof(nodeLinkedList));
	// Set it next values
	ptr_temp->info = ptr_Node->next->info;
	ptr_temp->next = ptr_Node->next->next;
	// Free the wanted node
	free(ptr_Node->next);
	// Connect it next
	ptr_Node->next = ptr_temp->next;
	return ptr_temp->info;
}


void add_Node_By_Line(nodeLinkedList  *list , void *info , int line)
{
    // Code section
    list = Find_Node_By_Line(list , line);
    (list -> next != NULL && list -> next -> line == line)? list -> next -> info = info  // The line already exists
        : addNode_List(list , info , line) ;
}

void delete_Node_By_Line(nodeLinkedList  *list , int line)
{
    // Code Section
    list = Find_Node_By_Line(list , line);
    (list -> next != NULL && list -> next -> line == line)? deleteNode_List(&list) : ZERO;
}


nodeLinkedList *Find_Node_By_Line(nodeLinkedList  *list , int line)
{
    // Code Section
    while(list->next != NULL && list->next->line < line)
    {
        list = list->next;
    }
    return list;
}

void createFromMemoFile_List(nodeLinkedList **list , FILE *file)
{
    // Variable Definition
    int Line_Counter = ONE;
    void *Temp_Info ;
    // Code Section
    init_List(list);
    while(!feof(file))
    {
        // Read line
        fread(Temp_Info ,sizeof(void *), ONE , file);
        // Add the line to the list
        addNode_List(*list , Temp_Info , Line_Counter++);
        // Move to the next node
        (*list) = (*list) -> next;
    }
}

void saveToFile_List(nodeLinkedList  *list , FILE *file)
{
    // Code Section
    while(list != NULL)
    {
        // Write current line to the file
        fwrite(list->info ,sizeof(void  *), ONE , file);
        list = list -> next;
    }

}

void free_List(nodeLinkedList **list)
{
    // Variable Definition
    nodeLinkedList *ptr_temp; // Temporary node

    // Code Section
	ptr_temp = (nodeLinkedList*)malloc(sizeof(nodeLinkedList));
	while((*list) != NULL)
    {
        ptr_temp = *list; // Hold the current node
        (*list) = (*list) -> next; // Move to the next node
        free(ptr_temp); // Free the previous node
    }
}

nodeLinkedList *getNextNode_List(nodeLinkedList  *list , nodeLinkedList *Node_Ptr)
{
    // Code Section
    return (Node_Ptr == NULL)? list : Node_Ptr -> next;
}

void copy_node(nodeLinkedList  *Source , nodeLinkedList  *Destination)
{
    // Code Section
    // Copy Source node to Destination node
    Destination -> line = Source -> line;
    Destination -> info = Source -> info;
    Destination -> next = Source -> next;

}

boolean getNodeByLine_List(nodeLinkedList  *list , nodeLinkedList  *helpNode , int line)
{
    // Variable Definition
    boolean Successfully_Copied;
    // Code Section
    list = Find_Node_By_Line(list , line); // Get the previous node
    Successfully_Copied = (list -> next != NULL && list -> next -> line == line)? TRUE , copy_node(list -> next , helpNode) :
        FALSE ;
    return Successfully_Copied;
}
